set(SUBSYS_NAME sdurws_lua)
set(SUBSYS_DESC "Interface for accessing RobWorkStudio from lua.")
set(SUBSYS_DEPS sdurws_robworkstudioapp RW::sdurw_lua)

set(build TRUE)

set(DEFAULT TRUE)
set(REASON)
if(NOT SWIG_FOUND)
    set(DEFAULT false)
    set(REASON "SWIG not found!")
else()

endif()

rw_subsys_option(
    build ${SUBSYS_NAME} ${SUBSYS_DESC} ${DEFAULT}
    REASON ${REASON}
    DEPENDS ${SUBSYS_DEPS}
    ADD_DOC
)

if(build)
    set(RWSIM_HAVE_LUA TRUE CACHE INTERNAL "")
    # MESSAGE(STATUS "SWIG found adding swig modules!")
    include(UseSWIG)

    set(CMAKE_SWIG_FLAGS "")

    set_source_files_properties(../sdurws.i PROPERTIES CPLUSPLUS ON)
    set_source_files_properties(../sdurws.i PROPERTIES SWIG_FLAGS "-includeall")
    include_directories(${RW_ROOT}/src)

    set(RWS_MODULE sdurws)
    set(TARGET_NAME ${RWS_MODULE}_lua)

    # ############ lua interface generation ##############
    if((CMAKE_VERSION VERSION_GREATER 3.8) OR (CMAKE_VERSION VERSION_EQUAL 3.8))
        swig_add_library(
            ${TARGET_NAME}
            TYPE SHARED
            LANGUAGE lua
            SOURCES ../sdurws.i Lua.cpp ../ScriptTypes.cpp
        )
    else()
        swig_add_module(${TARGET_NAME} lua ../sdurws.i Lua.cpp ../ScriptTypes.cpp)
    endif()

    if(NOT DEFINED MSVC) # This code is to make it easier to make debian packages
        set_target_properties(
            ${TARGET_NAME}
            PROPERTIES
                PREFIX ""
                OUTPUT_NAME ${RWS_MODULE}
                ARCHIVE_OUTPUT_DIRECTORY "${RWS_CMAKE_LIBRARY_OUTPUT_DIRECTORY}/Lua"
                LIBRARY_OUTPUT_DIRECTORY "${RWS_CMAKE_LIBRARY_OUTPUT_DIRECTORY}/Lua"
                LIBRARY_OUTPUT_DIRECTORY "${RWS_CMAKE_LIBRARY_OUTPUT_DIRECTORY}/Lua"
        )
    endif()
    swig_link_libraries(
        ${TARGET_NAME}
        ${SUBSYS_DEPS}
        ${RWS_PLUGIN_LIBRARIES}
        ${RWS_MODULE}
        RW::sdurw_lua
        RW::sdurw
    )
    if(RW_BUILD_WITH_INTERNAL_LUA)
        swig_link_libraries(${TARGET_NAME} RW::lua51)
    else()
        swig_link_libraries(${TARGET_NAME} ${RW_BUILD_WITH_LIBRARIES_LUA})
        target_include_directories(${TARGET_NAME} PUBLIC ${RW_BUILD_WITH_LUA_INCLUDE_DIR})
    endif()
    add_library(${PROJECT_PREFIX}::${TARGET_NAME} ALIAS ${TARGET_NAME})

    # ############ lua static interface generation ##############
    if((CMAKE_COMPILER_IS_GNUCC) OR (CMAKE_C_COMPILER_ID STREQUAL "Clang"))
        set_target_properties(${TARGET_NAME} PROPERTIES LINK_FLAGS -Wl,--no-undefined)
    endif()

    if((CMAKE_VERSION VERSION_GREATER 3.12.0) OR (CMAKE_VERSION VERSION_EQUAL 3.12.0))
        swig_add_library(
            ${TARGET_NAME}_s
            TYPE STATIC
            LANGUAGE lua
            SOURCES ../sdurws.i Lua.cpp ../ScriptTypes.cpp
        )
    else()
        add_library(${TARGET_NAME}_s STATIC ${swig_generated_sources} ${swig_other_sources})
    endif()

    target_link_libraries(
        ${TARGET_NAME}_s
        ${SUBSYS_DEPS}
        ${RWS_PLUGIN_LIBRARIES}
        ${RWS_MODULE}
        RW::sdurw_lua
        RW::sdurw
        RW::sdurw_lua_s
    )
    if(RW_BUILD_WITH_INTERNAL_LUA)
        target_link_libraries(${TARGET_NAME}_s RW::lua51)
    else()
        target_link_libraries(${TARGET_NAME}_s ${RW_BUILD_WITH_LIBRARIES_LUA})
        target_include_directories(${TARGET_NAME}_s PUBLIC ${RW_BUILD_WITH_LUA_INCLUDE_DIR})
    endif()
    add_library(${PROJECT_PREFIX}::${TARGET_NAME}_s ALIAS ${TARGET_NAME}_s)

    # Visual Studio or mingw this is used to indicate static linking to Visual Studio or mingw
    if(DEFINED MSVC)
        set_target_properties(${TARGET_NAME}_s PROPERTIES COMPILE_FLAGS "/DSTATIC_LINKED")
    else()
        set_target_properties(${TARGET_NAME}_s PROPERTIES COMPILE_FLAGS "-DSTATIC_LINKED")
    endif()

    add_library(sdurwslua_plugin.rwplugin MODULE LuaPlugin.cpp LuaPlugin.hpp)
    target_link_libraries(sdurwslua_plugin.rwplugin ${TARGET_NAME}_s RW::sdurw RW::sdurw_lua_s)
    install(TARGETS ${RWS_MODULE}lua_plugin.rwplugin DESTINATION ${RW_PLUGIN_INSTALL_DIR})

    if((CMAKE_COMPILER_IS_GNUCC) OR (CMAKE_C_COMPILER_ID STREQUAL "Clang"))
        set_target_properties(sdurwslua_plugin.rwplugin PROPERTIES LINK_FLAGS -Wl,--no-undefined)
    endif()

    # ############ INSTALL LUA LIBRARY #############################
    install(
        TARGETS ${TARGET_NAME}_s
        EXPORT ${PROJECT_PREFIX}Targets
        DESTINATION ${STATIC_LIB_INSTALL_DIR} COMPONENT swig
    )

    install(
        TARGETS ${TARGET_NAME} 
        EXPORT ${PROJECT_PREFIX}Targets 
        DESTINATION ${LUA_INSTALL_DIR} COMPONENT swig
    )
endif()
